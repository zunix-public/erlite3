# Edgerouter lite (erlite3) dhclient kernel priority 6
Patch dhclient pour compatibilité orange/sosh.

## Compiler dhclient
### Construire docker cross-compile
``` 
$ docker build -t mips docker-cross-compilation
```

### Compiler les sources dhclient
Exécuter le container :
``` 
$ docker run --rm -v $(pwd)/v2.0.9-hotfix.2/dhclient:/src -it mips /bin/bash
``` 

Compiler les sources :
``` 
$ cd vyatta-dhcp3_4.1-ESV-R15-ubnt1
$ make -f debian/rules configure
$ make distclean
$ CC=mips-linux-gnu-gcc CPP=mips-linux-gnu-cpp ./configure --host=mips-linux --cache-file=config.cache
$ make
``` 

## Préparer le routeur

### dhclient :
Envoyer fichier sur le serveur
```
scp client/dhclient ubtn@router:/tmp/
``` 

Remplacer le fichier sur le routeur
```
sudo cp /sbin/dhclient3 /sbin/dhclient3.bak
sudo mv /tmp/dhclient /sbin/dhclient3
sudo chown root:root /sbin/dhclient3
sudo chmod a+x /sbin/dhclient3
``` 
